// merge common config with either dev or prod
const { merge } = require('webpack-merge');
const commonConfig = require('./webpack.common.js');

// export the merged config
// by defining as function => gets access to env var in package.json

module.exports = (envVars) => {
  const { env } = envVars;

  // using env var, use appropriate config
  const envConfig = require(`./webpack.${env}.js`);

  // final config = merged common + env config
  const config = merge(commonConfig, envConfig);
  return config;
};
