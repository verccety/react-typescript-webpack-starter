const webpack = require('webpack');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

module.exports = {
  mode: 'development',
  devServer: {
    // enables React hot-module replacement
    hot: true,
    open: true,
  },
  devtool: 'cheap-module-source-map', // controls generation of source maps for our code,
  plugins: [
    new webpack.DefinePlugin({
      'process.env.name': JSON.stringify('Pavel'), // define our own env var; typically - for API endpoints, etc. could also use dotenv-webpack
    }),
    new ReactRefreshWebpackPlugin(),
  ],
};
