import './styles.css';
import IMAGE from './react-logo.png';
import LOGO from './logo.svg';
import { Counter } from './Counter';

export const App = () => {
  return (
    <>
      <h1>
        React TypeScript Webpack Starter Template - {process.env.NODE_ENV} {process.env.name}
      </h1>
      <img src={IMAGE} alt='React logo' width='300' height='200' />
      <img src={LOGO} alt='React logo' width='300' />
      <Counter />
    </>
  );
};
